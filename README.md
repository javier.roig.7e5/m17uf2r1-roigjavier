# M17UF2R1-RoigJavier

Controles:
	- Movimiento (W,A,S,D)
	- Disparo arma a distancia (Left Click)
		- Dispara en la dirección del cursor del mouse
	- Ataque a Melee (Right Click)
		- Tiene daño en area en las 4 direcciones a la vez
	- Dash (Space Bar)
		- El cooldown se ve reflejado en la barra azul con el símbolo del rayo que hay debajo de la barra de vida
	- Dash Attack (Space Bar + Right Click)
	- Cambio de arma con X y C
	- R para recargar el arma
	- Mientras el movimiento de Dash esta activo, el player no recibirá ningun tipo de daño


Inventario:
	- Dispones de 5 slots donde puedes almacenar items de curación y de revivir (las armas van a parte)
	- Puedes usar el icono de la cruz que hay encima de cada slot para soltar el objeto si hay otro que te interesa más coger
	- Hay que clicar con el ratón encima de del objeto en la barra de inventario para usarlo


Armas:
	- Todas son armas a distancia, tienen la misma cantidad de munición, cada una hace distinto daño, menos el IceStick que solo stunnea al enemigo durante 5 segundos.
	- La cadencia de disparo es diferente dependiendo del arma
	- Dispones de 7 balas en cada arma, si tienes un cargador extra puedes recargar a la tecla R. Solo se puede recargar cuando el cargador este vacío, cuando recoges un arma que ya tienes consigues un cargador extra (7 balas)


Items:
	- Salud:
		- Hay diferentes tipos de comida y cada una recupera una cantidad distinta de salut
		- Solo pueden consumirse cuando el usuario este por debajo del máximo de vida y este vivo.
	- Revivir:
		- cuando el jugador pierde toda la vida, dispone de un pequeño espacio de tiempo para tomar uno de estos items para revivir
		- Tienen el icono de un biberón con alas, hay dos variantes el normal resucita i recupera un poco de vida, el que tiene un icono de un signo "+" resucita i recupera casi toda la vida
		- Estos items solo se pueden consumir en el lapsus de tiempo antes de la muerte del jugador, que se indica en la pantalla del juego


Monedas:
	- Se consiguen como item recolectable o al derrotar enemigos / Boss, y superando el juego.
	- Pueden usarse para comprar personajes en la tienda


Obstáculos:
	- En algunas de las salas hay taquillas que sueltan un item aleatorio tras recibir tres impactos con cualquir arma


Enemigos:
	- Hay dos tipos básicos:
		- PlayerChaser: persigue al jugador hasta que esta suficientemente cerca para pegarle (solo detectará al jugador si esta dentro de cierto rango)
		- Turret: Disparara al jugador deade una posición estática, si el jugador se acerca también le pegará cuerpo a cuerpo (solo detectará al jugador si esta dentro de cierto rango)
	- Al morir dropean un objeto ramdom 


Boss:
	- Aparece en la última sala de la dungeon, hay que matarlo para ganar la partida
	- Tiene una posición estática desde la que dispara proyectiles al jugador
	- Tiene el doble de vida que el jugador
	- El jugador ha de estar dentro de cierto rango para que el Boss ataque
	- El Boss tiene dos fases, si tiene más de la mitad de la vida los ataques seran mas lentos y menos frequentes, si esta por debajo de la mitad de la vida habrá mucho menos tiempo entre disparos, siendo más dificiles de esquivar


EnemySpawner:
	- Serán de los dos tipos mencionados en el apartado enemigos, pero existen tres variantes estéticas que será alaeatoria en cada sala
	- Cuando entres a una sala se iniciaran las oleadas, son tres oleadas, en cada una pueden aparecer entre 1 o 4 enemigos a la vez, hay un pequeño cooldown a la hora de spawnear los enemigos
	- Los enemigos spawnean dentro de la sala en posiciones aleatorias


RoomSpawner:
	- Se generaran las salas de manera aleatoria y diferente en cada nivel
	- en la última sala generada estará el boss
	- en la sala del boss no habrá oleada de enemigos solo el boss
	- la sala inicial dispone de varios objetos para que inicies la partida


Ranking:
	- En el icono con una copa en la parte superior del menu de inicio, puedes acceder a ver el resultado de todas las partidas jugadas anteriormente


Persistencia de datos:
	- Se almacena aquellos personajes comprados en la tienda (no se pueden volver a comprar), los personajes disponibles para jugar(cuando los compras se añaden), la cantidad de dinero que tienes
	- También se almacenan las puntuaciones obtenidas en cada partida independientemente si gana o pierde, se guardan los puntos obtenidos, los enemigos eliminados y la fecha y hora de finalización de la partida

Detalles:
	- Si combinas el dash con el movimiento de ataque a melee cuando une enemigo este dentro del rango de daño, este recibirá el doble de daño
	- Los personajes en versión superhéroe (The Coon, Mysteion), tienen mayor alcance de daño en aréa con el ataque a melee
	- Hay que ejecutar el juego desde la escena de StartGame para poder seleccionar el personaje y que todo funcione bien
	- Obtienes puntos por recoger items del suelo, golper enemigos, matar enemigos, superar el nivel
	- Tanto si mueres como si ganas hay aparece un menu para volver a jugar o volver al menu principal
	- El HUD esta personalizado con respecto a cada jugador
	- El personaje de Kenny esta bugeado se recomienda no cogerlo, es funcional pero hay problemas en el apartado visual (no se ha podido retirar por complejidad a la hora de coordinar la tiendo con los personajes jugables y sus IDs)
	- Hay un bug con el ranking, los datos de las partidas se almacenan correctamente, pero a la hora de mostrarlo, muestra solo la última puntuación repetida tantas veces como resultados haya guardados
	


