using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickUp : MonoBehaviour
{
    private GameObject Coin;
    [SerializeField] private AudioClip PickUpSound;

    void Start()
    {
        Coin = GameObject.Find("_Coins");
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //Coin.Instance.Coins += 10;
            SoundController.Instance.PlaySound(PickUpSound);
            Coin.GetComponent<Coin>().AddCoins(10);
            Destroy(gameObject);
        }
    }
}
