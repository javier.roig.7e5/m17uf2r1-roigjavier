using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthItem : MonoBehaviour
{
    private PlayerData _playerData;
    private GameObject _player;

    private PlayerDamage _playerDamage;

    public float Bonus;

    [SerializeField] private AudioClip UseItemSound;

    /*public GameObject effect;
    private Transform player;*/

    void Start()
    {
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;
        _player = GameObject.Find(playerName + "(Clone)");

        _playerData = _player.GetComponent<PlayerData>();

        //player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void Use()
    {
        //Instantiate(effect, player.position, Quaternion.identity);
        _playerDamage = _player.GetComponent<PlayerDamage>();

        if (_playerDamage.AreDeath == false)
        {
            if (_playerData.Life < _playerData.MaxLife)
            {
                SoundController.Instance.PlaySound(UseItemSound);
                if (_playerData.Life + Bonus > _playerData.MaxLife)
                {
                    _playerData.Life = _playerData.MaxLife;
                }
                else
                {
                    _playerDamage.AddLife(Bonus);
                }
                Destroy(gameObject);
            }
        }
    }
}
