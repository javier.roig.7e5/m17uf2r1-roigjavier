using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWin : MonoBehaviour
{
    private GameObject _gameWin;
    private EnemiesDeathCount _enemiesDeathCount;
    private GameObject _deathEnemiesCounter;

    private PointsCounter _pointsCounter;
    private GameObject _score;
    private bool _pointsAdded = false;
    public bool _gameWinned = false;

    public GameObject Coins;

    [SerializeField] private AudioClip winSound;

    void Start()
    {
        _gameWin = GameObject.Find("GameWin");
        _gameWin.SetActive(false);
        _deathEnemiesCounter = GameObject.Find("DeathEnemiesCounter");
        _enemiesDeathCount = _deathEnemiesCounter.GetComponent<EnemiesDeathCount>();

        _score = GameObject.Find("Score");
        _pointsCounter = _score.GetComponent<PointsCounter>();

        Coins = GameObject.Find("_Coins");
    }

    void Update()
    {
        if (_gameWinned == true)
        {
            if (_pointsAdded == false)
            {
                _pointsCounter.AddPoints(100);
                _pointsAdded = true;
                Coins.GetComponent<Coin>().AddCoins(100);
                SoundController.Instance.PlaySound(winSound);
            }
            //Destroy(gameObject);
            _gameWin.SetActive(true);
        }
    }
}
