using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    private Collider2D Door;
    void Start()
    {
        Door = GetComponent<Collider2D>();
        //Door.isTrigger = false;
    }

    public void TriggerStatusOn()
    {
        Door.isTrigger = true;
    }

    public void TriggerStatusOff()
    {
        Door.isTrigger = false;
    }
}
