using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatorEnemieSpawner : MonoBehaviour
{
    public GameObject EnemieSpawner;
    void Start()
    {
        EnemieSpawner.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            EnemieSpawner.SetActive(true);
            Destroy(gameObject);
        }else if (other.gameObject.CompareTag("Boss"))
        {
            Destroy(EnemieSpawner);
            Destroy(gameObject);
        }
    }
}
