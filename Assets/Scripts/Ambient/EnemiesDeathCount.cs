using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesDeathCount : MonoBehaviour
{
    public int EnemyDeathCounter;
    private DoorScript _doorScript;
    private GameObject _door;
    private int EnemiesToEliminate;
    //public bool _gameWinned = false;

    private EnemySpawner _enemySpawnerScript;
    private GameObject _enemySpawner;

    void Start()
    {
        EnemyDeathCounter = 0;
        _door = GameObject.Find("DoorExit");

        _doorScript = _door.GetComponent<DoorScript>();
    }

    void Update()
    {
        if (EnemyDeathCounter == EnemiesToEliminate)
        {
            //_doorScript.TriggerStatusOn();
        }
    }

    public void CountKills()
    {
        EnemyDeathCounter++;
    }
}
