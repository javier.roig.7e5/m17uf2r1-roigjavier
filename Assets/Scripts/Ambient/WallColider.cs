using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallColider : MonoBehaviour
{
    private Collider2D Wall;

    void Start()
    {
        Wall = GetComponent<Collider2D>();
    }

    public void TriggerStatusOn()
    {
        Wall.isTrigger = true;
    }

    public void TriggerStatusOff()
    {
        Wall.isTrigger = false;
    }
}
