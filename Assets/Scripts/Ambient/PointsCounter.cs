using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PointsCounter : MonoBehaviour
{
    public float Score = 0;

    void Update()
    {
        this.gameObject.GetComponent<TMP_Text>().text = Convert.ToString("Score: " + Score);
    }

    public void AddPoints(float points)
    {
        Score += points;
    }

    public void SubtractPoints(float points)
    {
        Score -= points;
    }
}
