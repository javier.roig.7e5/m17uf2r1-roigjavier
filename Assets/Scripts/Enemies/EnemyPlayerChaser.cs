using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPlayerChaser : MonoBehaviour
{
    private PlayerData _playerData;
    private GameObject _player;
    public float ChasingSpeed;

    public bool ChaseAvailable = true;

    public float distanceBetween;
    [SerializeField]private float distance;

    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;
        _player = GameObject.Find(playerName + "(Clone)");
        _playerData = _player.GetComponent<PlayerData>();
    }

    
    void Update()
    {
        /*if (_playerData.Life > 0)
        {
            distanceBetween = 8;
            distance = Vector2.Distance(transform.position, _player.transform.position);
        }else
        {
            distance = 10;
        }*/

        if (ChaseAvailable)
        {
            distance = Vector2.Distance(transform.position, _player.transform.position);
            Vector2 direction = _player.transform.position - transform.position;
            direction.Normalize();
            animator.SetFloat("Horizontal", direction.x);
            animator.SetFloat("Vertical", direction.y);
            animator.SetFloat("Speed", direction.sqrMagnitude);

            animator.SetFloat("Distance", distance);
            if (distance < distanceBetween && _playerData.Life > 0)
            {


                transform.position = Vector2.MoveTowards(this.transform.position, _player.transform.position, ChasingSpeed * Time.deltaTime);
            }
            else if (_playerData.Life < 1)
            {
                distanceBetween = 0;
                distance = 10;
            }
        }
        else
        {
            animator.SetFloat("Distance", distance);
            distance = 20;
        }
        
        
    }
}
