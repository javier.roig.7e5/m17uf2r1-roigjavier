using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    private enum AttackStages
    {
        stage1,
        stage2,
    }

    private EnemiesDeathCount _enemiesDeathCountScript;
    private GameObject _deathCounter;

    public float HitPoints;
    public float MaxHitPoints = 5;
    public EnemyHealthBar enemyHealthBar;

    private PointsCounter _pointsCounter;
    private GameObject _score;

    public float HittedPoints;
    public float KilledPoints;

    private Animator animator;

    private GameObject ItemDropper;
    private ItemDropSpawner itemDropSpawner;

    [SerializeField] private float _loseControleTime;

    private Renderer rend;
    [SerializeField]
    private Color colorToTurnTo = Color.white;
    [SerializeField]
    private Color colorToTurnTo2 = Color.white;
    [SerializeField]
    private Color colorToTurnTo3 = Color.white;

    [SerializeField] private AudioClip HittedSound;

    private PlayerData _playerData;
    private GameObject _player;

    public GameObject projectile;
    public GameObject projectileVariant;
    public Transform firePoint;
    [SerializeField] public float projectileSpeed = 10;
    [SerializeField]
    private float _shootCadenceTime;

    float lookAngle;

    private bool _shootAvailable = true;

    public float distanceBetween;
    private float distance;

    public bool AttackAvailable = true;

    [SerializeField] private AudioClip ShotSound;

    [SerializeField] private AttackStages BossStatus;

    private GameObject _gameWin;

    private void Start()
    {
        BossStatus = AttackStages.stage1;
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;
        _player = GameObject.Find(playerName + "(Clone)");
        _playerData = _player.GetComponent<PlayerData>();

        animator = GetComponent<Animator>();
        HitPoints = MaxHitPoints;
        enemyHealthBar.SetHealth(HitPoints, MaxHitPoints);
        _deathCounter = GameObject.Find("DeathEnemiesCounter");
        _enemiesDeathCountScript = _deathCounter.GetComponent<EnemiesDeathCount>();

        ItemDropper = GameObject.Find("ItemDropSpawner");
        itemDropSpawner = ItemDropper.GetComponent<ItemDropSpawner>();

        _score = GameObject.Find("Score");
        _pointsCounter = _score.GetComponent<PointsCounter>();

        _gameWin = GameObject.Find("WinController");
    }

    void Update()
    {
        animator.SetFloat("Life", HitPoints);
        if (HitPoints < 100)
        {
            BossStatus = AttackStages.stage2;
        }

        if (AttackAvailable)
        {
            distance = Vector2.Distance(transform.position, _player.transform.position);

            if (distance < distanceBetween && _playerData.Life > 0)
            {
                Vector2 lookDirection = _player.transform.position - transform.position;

                lookAngle = Mathf.Atan2(lookDirection.y, (lookDirection.x)) * Mathf.Rad2Deg;

                firePoint.rotation = Quaternion.Euler(0, 0, lookAngle);

                animator.SetFloat("Horizontal", lookDirection.x);
                animator.SetFloat("Vertical", lookDirection.y);


                if (_shootAvailable)
                {
                    SoundController.Instance.PlaySound(ShotSound);
                    GameObject projectileClone;
                    if (BossStatus == AttackStages.stage1)
                    {
                        projectileClone = Instantiate(projectile);
                    }
                    else
                    {
                        projectileClone = Instantiate(projectileVariant);
                        _shootCadenceTime = 0.5f;
                    }
                    StartCoroutine(Cadence());
                    
                    projectileClone.transform.position = firePoint.position;
                    projectileClone.transform.rotation = Quaternion.Euler(0, 0, lookAngle);

                    projectileClone.GetComponent<Rigidbody2D>().velocity = firePoint.right * projectileSpeed;
                    animator.SetTrigger("Attack");
                }
            }
        }
        else
        {
            distance = 20;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            animator.SetTrigger("Hit");
            if (GameObject.Find("ToyArrow(Clone)"))
            {
                TakeHit(5);
            }
            else if(GameObject.Find("Arrow(Clone)")) 
            {
                TakeHit(25);
            }
            else if (GameObject.Find("BlueEnergyFireBall(Clone)"))
            {
                TakeHit(20);
            }
            else if (GameObject.Find("LaserProjectile(Clone)"))
            {
                TakeHit(10);
            }
            else if (GameObject.Find("NinjaStar(Clone)"))
            {
                TakeHit(15);
            }
            else if (GameObject.Find("Snowball(Clone)"))
            {
                StartCoroutine(Stunned());
            }
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            animator.SetTrigger("Attack");

        }

    }

    public void TakeHit(float hitTaken)
    {
        SoundController.Instance.PlaySound(HittedSound);
        HitPoints -= hitTaken;
        enemyHealthBar.SetHealth(HitPoints, MaxHitPoints);
        _pointsCounter.AddPoints(HittedPoints);
        if (HitPoints < 1)
        {
            _enemiesDeathCountScript.CountKills();
            _pointsCounter.AddPoints(KilledPoints);
            itemDropSpawner.DropItem(new Vector3(transform.position.x, transform.position.y, 0));
            Destroy(gameObject);
            _gameWin.GetComponent<GameWin>()._gameWinned = true;

        }
        StartCoroutine(LoseControl());
    }

    private IEnumerator LoseControl()
    {
        rend = GetComponent<Renderer>();
        rend.material.color = colorToTurnTo;
        yield return new WaitForSeconds(_loseControleTime);
        rend.material.color = colorToTurnTo2;
    }

    private IEnumerator Stunned()
    {
        rend = GetComponent<Renderer>();
        rend.material.color = colorToTurnTo3;
        AttackAvailable = false;
        yield return new WaitForSeconds(5);
        rend.material.color = colorToTurnTo2;
        AttackAvailable = true;
    }

    private IEnumerator Cadence()
    {
        _shootAvailable = false;
        yield return new WaitForSeconds(_shootCadenceTime);
        _shootAvailable = true;
    }
}
