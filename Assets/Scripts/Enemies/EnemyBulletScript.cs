using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletScript : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Door"))
        {
            GetComponent<BoxCollider2D>().size = new Vector2(4, 4);

            DestroyEnemyBullet();
        }
    }

    public void DestroyEnemyBullet()
    {
        Destroy(gameObject);
    }
}
