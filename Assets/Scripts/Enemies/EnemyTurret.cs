using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTurret : MonoBehaviour
{
    private PlayerData _playerData;
    private GameObject _player;

    public GameObject projectile;
    public Transform firePoint;
    [SerializeField ]public float projectileSpeed = 10;
    [SerializeField]
    private float _shootCadenceTime;

    float lookAngle;

    private bool _shootAvailable = true;

    public float distanceBetween;
    private float distance;

    private Animator animator;

    public bool TurretAvailable = true;

    [SerializeField] private AudioClip ShotSound;
    private void Start()
    {
        animator = GetComponent<Animator>();
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;
        _player = GameObject.Find(playerName + "(Clone)");

        _playerData = _player.GetComponent<PlayerData>();
    }
    void Update()
    {
        if (TurretAvailable)
        {
            distance = Vector2.Distance(transform.position, _player.transform.position);

            if (distance < distanceBetween && _playerData.Life > 0)
            {
                Vector2 lookDirection = _player.transform.position - transform.position;

                lookAngle = Mathf.Atan2(lookDirection.y, (lookDirection.x)) * Mathf.Rad2Deg;

                firePoint.rotation = Quaternion.Euler(0, 0, lookAngle);

                animator.SetFloat("Horizontal", lookDirection.x);
                animator.SetFloat("Vertical", lookDirection.y);


                if (_shootAvailable)
                {
                    SoundController.Instance.PlaySound(ShotSound);
                    StartCoroutine(Cadence());
                    GameObject projectileClone = Instantiate(projectile);
                    projectileClone.transform.position = firePoint.position;
                    projectileClone.transform.rotation = Quaternion.Euler(0, 0, lookAngle);

                    projectileClone.GetComponent<Rigidbody2D>().velocity = firePoint.right * projectileSpeed;
                    animator.SetTrigger("Attack");
                }
            }
        }
        else
        {
            distance = 20;
        }
    }

    private IEnumerator Cadence()
    {
        _shootAvailable = false;
        yield return new WaitForSeconds(_shootCadenceTime);
        _shootAvailable = true;
    }
}
