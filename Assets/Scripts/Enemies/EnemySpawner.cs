using System.Linq;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    private enum Waves
    {
        wave0,
        wave1,
        wave2,
        wave3
    }

    private float minX, maxX, minY, maxY;
    [SerializeField] private Transform[] points;
    //[SerializeField] private GameObject[] enemys;
    [SerializeField] private float timeEnemys;

    [SerializeField] private GameObject[] Tardiaca;
    [SerializeField] private GameObject[] CrabPeople;
    [SerializeField] private GameObject[] Gotics;
    public int SpawnLimit = 0;
    public bool SpawmEnemyAvailable = true;
    [SerializeField] private int _enemiesSpawnedCounter = 0;
    [SerializeField]private Waves wavesStatus;
    [SerializeField] private bool waveSetted;
    [SerializeField] private bool waveInProgress;
    [SerializeField] private bool wavesCompleted;

    private GameObject enemieDeathCount;

    [SerializeField] private int wavesCounter = 0;

    private int initKills = 0, finalKills = 0;

    private int enemyType;


    private float timeNextEnemys;
    private void Start()
    {
        enemyType = ChooseEnemyType();
        wavesStatus = Waves.wave0;
        waveSetted = false;
        waveInProgress = false;
        wavesCompleted = false;
        enemieDeathCount = GameObject.Find("DeathEnemiesCounter");

        maxX = points.Max(point => point.position.x);
        minX = points.Min(point => point.position.x);
        maxY = points.Max(point => point.position.y);
        minY = points.Min(point => point.position.y);
    }

    private void Update()
    {
        if (wavesCompleted == false)
        {
            if (enemieDeathCount.GetComponent<EnemiesDeathCount>().EnemyDeathCounter == initKills + (SpawnLimit + 1))
            {
                finalKills = enemieDeathCount.GetComponent<EnemiesDeathCount>().EnemyDeathCounter;
            }

            if (finalKills - initKills == SpawnLimit + 1)
            {
                waveInProgress = false;
                waveSetted = false;
                SpawmEnemyAvailable = true;
                _enemiesSpawnedCounter = 0;
                SpawnLimit = 0;
                initKills = 0;
                finalKills = 0;
                wavesCounter++;
            }

            if (wavesStatus == Waves.wave3 && wavesCounter > 1)
            {
                wavesCompleted = true;
                Destroy(gameObject);
            }

            if (waveInProgress == false)
            {
                if (wavesStatus == Waves.wave0)
                {
                    wavesStatus = Waves.wave1;
                }
                else if (wavesStatus == Waves.wave1)
                {
                    wavesStatus = Waves.wave2;
                }
                else if (wavesStatus == Waves.wave2)
                {
                    wavesStatus = Waves.wave3;
                }
                waveInProgress = true;
                initKills = enemieDeathCount.GetComponent<EnemiesDeathCount>().EnemyDeathCounter;
            }

            if (waveSetted == false)
            {
                SpawnLimit = Random.Range(0, 3);
                waveSetted = true;
            }

            if (waveInProgress == true)
            {
                timeNextEnemys += Time.deltaTime;

                if (_enemiesSpawnedCounter > SpawnLimit)
                {
                    SpawmEnemyAvailable = false;
                }

                if (timeNextEnemys >= timeEnemys && SpawmEnemyAvailable)
                {
                    timeNextEnemys = 0;
                    switch (enemyType)
                    {
                        case 1:
                            CreateEnemy(Tardiaca);
                            break;
                        case 2:
                            CreateEnemy(CrabPeople);
                            break;
                        case 3:
                            CreateEnemy(Gotics);
                            break;
                        default:
                            CreateEnemy(CrabPeople);
                            break;
                    }
                    _enemiesSpawnedCounter++;
                }
            }
        }
    }

    private void CreateEnemy(GameObject [] EnemyArray)
    {
        int enemyNumber = Random.Range(0, EnemyArray.Length);
        Vector2 randomPosition = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));

        Instantiate(EnemyArray[enemyNumber], randomPosition, Quaternion.identity);
    }

    private int ChooseEnemyType()
    {
        int enemyTypeNumber = Random.Range(0, 4);
        switch (enemyTypeNumber)
        {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            default:
                return 2;
        }
    }
}
