using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private EnemiesDeathCount _enemiesDeathCountScript;
    private GameObject _deathCounter;

    public float HitPoints;
    public float MaxHitPoints = 5;
    public EnemyHealthBar enemyHealthBar;

    private PointsCounter _pointsCounter;
    private GameObject _score;

    public float HittedPoints;
    public float KilledPoints;

    private Animator animator;

    private GameObject ItemDropper;
    private ItemDropSpawner itemDropSpawner;

    private EnemyPlayerChaser enemyPlayerChaser;


    /*
    private EnemyPlayerChaser enemyPlayerChaser;*/
    [SerializeField] private float _loseControleTime;

    private Renderer rend;
    [SerializeField]
    private Color colorToTurnTo = Color.white;
    [SerializeField]
    private Color colorToTurnTo2 = Color.white;
    [SerializeField]
    private Color colorToTurnTo3 = Color.white;

    [SerializeField] private AudioClip HittedSound;

    private void Start()
    {
        animator = GetComponent<Animator>();
        HitPoints = MaxHitPoints;
        enemyHealthBar.SetHealth(HitPoints, MaxHitPoints);
        _deathCounter = GameObject.Find("DeathEnemiesCounter");
        _enemiesDeathCountScript = _deathCounter.GetComponent<EnemiesDeathCount>();

        ItemDropper = GameObject.Find("ItemDropSpawner");
        itemDropSpawner = ItemDropper.GetComponent<ItemDropSpawner>();

        _score = GameObject.Find("Score");
        _pointsCounter = _score.GetComponent<PointsCounter>();

        //enemyPlayerChaser = GetComponent<EnemyPlayerChaser>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            animator.SetTrigger("Hit");
            //HitPoints--;
            if (GameObject.Find("ToyArrow(Clone)"))
            {
                TakeHit(5);
            }
            else if(GameObject.Find("Arrow(Clone)")) 
            {
                TakeHit(25);
            }
            else if (GameObject.Find("BlueEnergyFireBall(Clone)"))
            {
                TakeHit(20);
            }
            else if (GameObject.Find("LaserProjectile(Clone)"))
            {
                TakeHit(10);
            }
            else if (GameObject.Find("NinjaStar(Clone)"))
            {
                TakeHit(15);
            }
            else if (GameObject.Find("Snowball(Clone)"))
            {
                StartCoroutine(Stunned());
                //TakeHit(0);
            }

            //EnemyTakeDamageRebound(1, collision.GetContact(0).normal);
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            //HitPoints--;
            //TakeHit(1);
            //collision.gameObject.GetComponent<PlayerDamage>().TakeDamageRebound(1, collision.GetContact(0).normal);
            animator.SetTrigger("Attack");

        }

    }

    public void TakeHit(float hitTaken)
    {
        SoundController.Instance.PlaySound(HittedSound);
        HitPoints -= hitTaken;
        enemyHealthBar.SetHealth(HitPoints, MaxHitPoints);
        _pointsCounter.AddPoints(HittedPoints);
        if (HitPoints < 1)
        {
            _enemiesDeathCountScript.CountKills();
            _pointsCounter.AddPoints(KilledPoints);
            itemDropSpawner.DropItem(new Vector3(transform.position.x, transform.position.y, 0));
            Destroy(gameObject);

        }
        StartCoroutine(LoseControl());
    }

    /*public void EnemyTakeDamageRebound(float damageTaken, Vector2 position)
    {
        HitPoints -= damageTaken;
        enemyHealthBar.SetHealth(HitPoints, MaxHitPoints);
        _pointsCounter.AddPoints(HittedPoints);
        if (HitPoints < 1)
        {
            _enemiesDeathCountScript.CountKills();
            _pointsCounter.AddPoints(KilledPoints);
            Destroy(gameObject);

        }
        StartCoroutine(LoseControl());
        enemyPlayerChaser.EnemyRebound(position);
    }*/

    private IEnumerator LoseControl()
    {
        rend = GetComponent<Renderer>();
        //enemyPlayerChaser._moveAvailable = false;
        rend.material.color = colorToTurnTo;
        yield return new WaitForSeconds(_loseControleTime);
        //enemyPlayerChaser._moveAvailable = true;
        rend.material.color = colorToTurnTo2;
    }

    private IEnumerator Stunned()
    {
        rend = GetComponent<Renderer>();
        //enemyPlayerChaser._moveAvailable = false;
        rend.material.color = colorToTurnTo3;
        if (this.GetComponent<EnemyPlayerChaser>())
        {
            this.GetComponent<EnemyPlayerChaser>().ChaseAvailable = false;
        }
        else if (this.GetComponent<EnemyTurret>())
        {
            this.GetComponent<EnemyTurret>().TurretAvailable = false;
        }
        yield return new WaitForSeconds(5);
        rend.material.color = colorToTurnTo2;
        if (this.GetComponent<EnemyPlayerChaser>())
        {
            this.GetComponent<EnemyPlayerChaser>().ChaseAvailable = true;
        }
        else if (this.GetComponent<EnemyTurret>())
        {
            this.GetComponent<EnemyTurret>().TurretAvailable = true;
        }
    }
}
