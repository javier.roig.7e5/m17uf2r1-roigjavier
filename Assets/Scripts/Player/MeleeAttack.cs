using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
    [SerializeField] private Transform hitController;
    [SerializeField] private float hitRadius;
    [SerializeField] private float hitDamage;
    private float hitDamageContainer;
    [SerializeField] private float timeBetweenAttacks;
    [SerializeField] private float timeNextAttack;
    private Animator _animator;

    private PlayerMove _playerMove;

    /*private GameObject _enemyBullet;
    private EnemyBulletScript _enemyBulletScript;*/

    void Start()
    {
        _animator = GetComponent<Animator>();
        _playerMove = GetComponent<PlayerMove>();

        hitDamageContainer = hitDamage;

        /*_enemyBullet = GameObject.Find("EnemyProjectile(Clone)");

        _enemyBulletScript = _enemyBullet.GetComponent<EnemyBulletScript>();*/
    }

    void Update()
    {
        if (timeNextAttack > 0)
        {
            timeNextAttack -= Time.deltaTime;
        }

        if (Input.GetButtonDown("Fire2") && timeNextAttack <= 0)
        {
            Debug.Log("ATACAO");
            Hit();
            timeNextAttack = timeBetweenAttacks;
        }
    }

    private void Hit()
    {
        _animator.SetTrigger("Hit");
        Collider2D[] objects = Physics2D.OverlapCircleAll(hitController.position, hitRadius);

        foreach(Collider2D collisioner in objects)
        {
            if (collisioner.CompareTag("Enemy"))
            {
                if (_playerMove._dashAvailable == false)
                {
                    hitDamage = hitDamage * 2;
                }
                collisioner.transform.GetComponent<Enemy>().TakeHit(hitDamage);
                hitDamage = hitDamageContainer;
            }/*else if (collisioner.CompareTag("EnemyProjectile"))
            {
                _enemyBulletScript.DestroyEnemyBullet();
            }*/
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(hitController.position, hitRadius);
    }
}
