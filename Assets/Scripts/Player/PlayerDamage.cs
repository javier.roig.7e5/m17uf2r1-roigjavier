using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : MonoBehaviour
{
    private PlayerData _playerData;
    private GameObject _player;
    public bool DamageAvailable = true;
    public bool AreDeath = false;

    private PlayerMove _playerMove;
    [SerializeField] private float _loseControleTime;

    private GameObject _reviveScreen;
    private GameObject _gameOver;

    private Renderer rend;
    [SerializeField]
    private Color colorToTurnTo = Color.white;
    [SerializeField]
    private Color colorToTurnTo2 = Color.white;

    [SerializeField] private AudioClip deathSound;
    [SerializeField] private AudioClip HittedSound;
    void Start()
    {
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;
        _player = GameObject.Find(playerName + "(Clone)");

        _playerData = _player.GetComponent<PlayerData>();

        _playerMove = GetComponent<PlayerMove>();

        _reviveScreen = GameObject.Find("ReviveScreen");
        _reviveScreen.SetActive(false);
        _gameOver = GameObject.Find("GameOver");
        _gameOver.SetActive(false);
    }

    void Update()
    {
        if (_playerData.Life < 1)
        {
            if (AreDeath == false)
            {
                StartCoroutine(ReviveTime());
            }
        }else if (_playerData.Life > 0.9)
        {
            AreDeath = false;
            _gameOver.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            TakeDamageRebound(20, collision.GetContact(0).normal);

        }else if (collision.gameObject.CompareTag("EnemyProjectile"))
        {
            TakeDamageRebound(10, collision.GetContact(0).normal);
        }

    }

    public void TakeDamage(float damageTaken)
    {
        if (DamageAvailable)
        {
            _playerData.Life -= damageTaken;
        }
        
    }

    public void TakeDamageRebound(float damageTaken, Vector2 position)
    {
        if (DamageAvailable)
        {
            SoundController.Instance.PlaySound(HittedSound);
            _playerData.Life -= damageTaken;
            StartCoroutine(LoseControl());
            _playerMove.Rebound(position);
        }
    }

    public void AddLife(float addedLife)
    {
        _playerData.Life += addedLife;
    }

    private IEnumerator LoseControl()
    {
        rend = GetComponent<Renderer>();
        _playerMove._moveAvailable = false;
        rend.material.color = colorToTurnTo;
        yield return new WaitForSeconds(_loseControleTime);
        _playerMove._moveAvailable = true;
        rend.material.color = colorToTurnTo2;
    }

    private IEnumerator ReviveTime()
    {
        //_playerMove._moveAvailable = false;
        AreDeath = true;
        _reviveScreen.SetActive(true);
        yield return new WaitForSeconds(4);
        _reviveScreen.SetActive(false);
        if (_playerData.Life < 1)
        {
            //AreDeath = true;
            _gameOver.SetActive(true);
            SoundController.Instance.PlaySound(deathSound);
        }
        else
        {
            AreDeath = false;
        }
        //_playerMove._moveAvailable = true;
    }
}
