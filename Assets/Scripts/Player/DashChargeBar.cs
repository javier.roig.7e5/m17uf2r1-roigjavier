using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashChargeBar : MonoBehaviour
{
    private PlayerData _playerData;
    private PlayerMove _playerMove;
    private GameObject _player;
    public Image fillImage;
    private Slider slider;

    public float DashBarMax = 3;
    public float DashBarCurrent = 3;

    void Awake()
    {
        slider = GetComponent<Slider>();
    }

    void Start()
    {
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;
        _player = GameObject.Find(playerName + "(Clone)");
        _playerData = _player.GetComponent<PlayerData>();
        _playerMove = _player.GetComponent<PlayerMove>();
    }

    void Update()
    {
        if (slider.value <= slider.minValue)
        {
            fillImage.enabled = false;
        }

        if (slider.value > slider.minValue && !fillImage.enabled)
        {
            fillImage.enabled = true;
        }
        float fillValue = DashBarCurrent / DashBarMax;
        slider.value = fillValue;
    }

    void FixedUpdate()
    {
        if (_playerMove._dashAvailable == false)
        {
            if (DashBarCurrent > 1)
            {
                DashBarCurrent -= 1f;
            }
            
        }else if (_playerMove._dashAvailable == true)
        {
            if (DashBarCurrent < 3)
            {
                DashBarCurrent += 0.009f;
            }
            
        }
    }
}
