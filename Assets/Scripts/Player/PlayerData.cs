using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    [SerializeField]
    private string CharacterName;

    public float speed = 5;
    public float rotationSpeed = 5;
    public float MaxLife;
    public float Life;
    private Animator playerAnimator;


    void Start()
    {
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        CharacterName = GameManager.Instance.characters[playerIndex].name;
        playerAnimator = GetComponent<Animator>();
    }

    void Update()
    {
        playerAnimator.SetFloat("Life", Life);
    }

}
