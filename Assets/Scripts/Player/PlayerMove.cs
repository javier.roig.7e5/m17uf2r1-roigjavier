using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public float rotationOffset;

    private PlayerData _playerData;
    private GameObject _player;
    private PlayerDamage _playerDamage;
    private Rigidbody2D playerRb;
    private Vector2 moveInput;
    private Vector3 mousePos;
    private Animator playerAnimator;
    public TrailRenderer TrailRenderer;
    private DashChargeBar _dashChargeBarScript;
    private GameObject _dashChargeBar;

    [SerializeField]
    private float _dashSpeed;
    [SerializeField]
    private float _dashTime;
    private float _initialGravity;
    public bool _dashAvailable = true;
    public bool _moveAvailable = true;
    [SerializeField]
    private Vector2 _reboundVelocity;
    void Start()
    {
        _playerData = GetComponent<PlayerData>();
        playerRb = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();

        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;
        _player = GameObject.Find(playerName + "(Clone)");

        _playerDamage = _player.GetComponent<PlayerDamage>();

        _dashChargeBar = GameObject.Find("DashChargeBar");
        _dashChargeBarScript = _dashChargeBar.GetComponent<DashChargeBar>();
    }

    void Update()
    {
        if (_playerData.Life > 0)
        {
            playerRb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.None | RigidbodyConstraints2D.None;
            mousePos = Input.mousePosition;
            mousePos.z = 0;
            Vector3 objectpos = Camera.main.WorldToScreenPoint(transform.position);

            mousePos.x = mousePos.x - objectpos.x;
            mousePos.y = mousePos.y - objectpos.y;

            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0 + rotationOffset));

            float moveX = Input.GetAxisRaw("Horizontal");
            float moveY = Input.GetAxisRaw("Vertical");
            moveInput = new Vector2(moveX, moveY).normalized;

            playerAnimator.SetFloat("Horizontal", mousePos.x);
            playerAnimator.SetFloat("Vertical", mousePos.y);
            playerAnimator.SetFloat("Speed", mousePos.sqrMagnitude);

            playerAnimator.SetFloat("MoveSpeed", moveInput.sqrMagnitude);

            if (Input.GetKeyDown(KeyCode.Space) && _dashAvailable)
            {
                if (_dashChargeBarScript.DashBarCurrent > 2.9)
                {
                    StartCoroutine(Dash());
                }
                

            }
        }
        else
        {
            //playerRb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
            playerRb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        }
    }

    private void FixedUpdate()
    {
        if (_moveAvailable && _playerData.Life > 0)
        {
            playerRb.MovePosition(playerRb.position + moveInput * _playerData.speed * Time.fixedDeltaTime);
        }

    }

    private IEnumerator Dash()
    {
        _moveAvailable = false;
        _dashAvailable = false;
        playerRb.gravityScale = 0;
        playerRb.velocity = new Vector2((mousePos.x - transform.position.x), (mousePos.y - transform.position.y)).normalized * _dashSpeed;
        TrailRenderer.emitting = true;
        _playerDamage.DamageAvailable = false;
        yield return new WaitForSeconds(_dashTime);
        _moveAvailable = true;
        _dashAvailable = true;
        playerRb.gravityScale = _initialGravity;
        TrailRenderer.emitting = false;
        _playerDamage.DamageAvailable = true;
    }


    public void Rebound(Vector2 hitPoint)
    {
        playerRb.velocity = new Vector2(-_reboundVelocity.x * hitPoint.x, _reboundVelocity.y);
    }
}
