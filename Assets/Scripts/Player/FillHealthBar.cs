using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillHealthBar : MonoBehaviour
{
    private PlayerData _playerData;
    private GameObject _player;
    public Image fillImage;
    private Slider slider;

    void Awake()
    {
        slider = GetComponent<Slider>();
    }

    void Start()
    {
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;
        _player = GameObject.Find(playerName + "(Clone)");
        _playerData = _player.GetComponent<PlayerData>();
    }

    void Update()
    {
        if (slider.value <= slider.minValue)
        {
            fillImage.enabled = false;
        }

        if (slider.value > slider.minValue && !fillImage.enabled)
        {
            fillImage.enabled = true;
        }
        float fillValue = _playerData.Life / _playerData.MaxLife;
        slider.value = fillValue;
    }
}
