using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeSon : MonoBehaviour
{
    //private GameObject object1;
    private GameObject _player;
    private GameObject objectVcamFollowPoint;
    
    void Start()
    {
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;
        _player = GameObject.Find(playerName + "(Clone)");
        //object1 = GameObject.Find("Object1");
        objectVcamFollowPoint = GameObject.Find("VcamFollowPoint");
        objectVcamFollowPoint.transform.parent = _player.transform;
    }
}
