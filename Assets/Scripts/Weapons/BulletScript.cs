using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Door") || collision.gameObject.CompareTag("Obstacle") || collision.gameObject.CompareTag("Boss"))
        {
            GetComponent<BoxCollider2D>().size = new Vector2(4, 4);

            Destroy(gameObject);
        }
    }
}
