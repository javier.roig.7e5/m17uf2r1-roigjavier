using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCollected : MonoBehaviour
{
    //private PlayerData _playerData;
    private GameObject _player;

    private GameObject weaponFound;
    private WeaponShooting weaponShooting;
    public string WeaponName;
    public GameObject Weapon;
    public float xSpawnPosition;
    public float ySpawnPosition;
    private GameObject GunHolder;

    private PointsCounter _pointsCounter;
    private GameObject _score;

    void Start()
    {
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;
        _player = GameObject.Find(playerName + "(Clone)");

        //_playerData = _player.GetComponent<PlayerData>();

        _score = GameObject.Find("Score");
        _pointsCounter = _score.GetComponent<PointsCounter>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            weaponFound = GameObject.Find(WeaponName + "(Clone)");
            if (weaponFound)
            {
                weaponShooting = weaponFound.GetComponent<WeaponShooting>();
                weaponShooting.ChargersQuantity += 1;
            }
            else
            {
                GunHolder = GameObject.Find("GunHolder");
                Instantiate(Weapon);
                weaponFound = GameObject.Find(WeaponName + "(Clone)");
                weaponFound.transform.position = new Vector3(_player.transform.position.x + xSpawnPosition, _player.transform.position.y + ySpawnPosition, 0);
                weaponFound.transform.parent = GunHolder.transform;
                
            }

            _pointsCounter.AddPoints(5);
            Destroy(gameObject);
        }
    }
}
