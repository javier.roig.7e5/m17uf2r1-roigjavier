using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponShooting : MonoBehaviour
{
    private PlayerData PlayerData;
    private GameObject _player;

    public GameObject projectile;
    public Transform firePoint;
    public float projectileSpeed = 50;
    [SerializeField]
    private float _countdown;
    private float _count;
    public int BulletsQuantity;
    public int Charger;
    public int ChargersQuantity;
    //public float fireCadence;
    //public float RecoilForce;

    Vector2 lookDirection;
    float lookAngle;

    [SerializeField] private AudioClip ShotSound;
    private void Start()
    {
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;
        _player = GameObject.Find(playerName + "(Clone)");

        _count = _countdown;
        PlayerData = _player.GetComponent<PlayerData>();
    }
    void Update()
    {
        if (PlayerData.Life > 0)
        {
            lookDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            lookAngle = Mathf.Atan2(lookDirection.y, (lookDirection.x)) * Mathf.Rad2Deg;

            firePoint.rotation = Quaternion.Euler(0, 0, lookAngle);

            if (Input.GetMouseButtonDown(0) && _count >= _countdown)
            {
                if (BulletsQuantity > 0)
                {
                    SoundController.Instance.PlaySound(ShotSound);
                    GameObject projectileClone = Instantiate(projectile);
                    projectileClone.transform.position = firePoint.position;
                    projectileClone.transform.rotation = Quaternion.Euler(0, 0, lookAngle);

                    projectileClone.GetComponent<Rigidbody2D>().velocity = firePoint.right * projectileSpeed;
                    _count = 0;
                    BulletsQuantity--;
                }

            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                if (BulletsQuantity == 0)
                {
                    if (ChargersQuantity > 0)
                    {
                        BulletsQuantity = Charger;
                        ChargersQuantity--;
                    }
                }
            }
            _count += Time.deltaTime;
        }

        

    }
}
