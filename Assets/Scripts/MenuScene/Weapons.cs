using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewWeapon", menuName = "Weapon")]
public class Weapons : ScriptableObject
{
    public GameObject Bullet;
    //public GameObject lifeBar;

    public Sprite image;

    public string name;

    public int BulletsQuantity;
    public int Charger;
    public int ChargersQuantity;
    public float projectileSpeed;
    public float fireCadence;
    public float RecoilForce;
    public float Damage;
}
