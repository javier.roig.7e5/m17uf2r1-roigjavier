using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    public GameObject InventoryCanvas;
    //public GameObject Hud;
    private void Awake()
    {
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        Instantiate(GameManager.Instance.characters[playerIndex].playableCharacter, transform.position, Quaternion.identity);
        Instantiate(GameManager.Instance.characters[playerIndex].lifeBar, transform.position, Quaternion.identity);
        //Instantiate(InventoryCanvas);
        //string playerName = GameManager.Instance.characters[playerIndex].name;

        //Instantiate(Hud);
    }
}
