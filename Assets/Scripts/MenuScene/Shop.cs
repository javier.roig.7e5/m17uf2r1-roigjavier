using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Shop : MonoBehaviour
{
    #region Singlton:Shop

	public static Shop Instance;

	void Awake ()
	{
		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);
	}

	#endregion
    [System.Serializable]
    class ShopItem
    {
        public Sprite Image;
        public int Price;
        public bool IsPurchased = false;
    }

    [SerializeField] List<ShopItem> ShopItemsList;
    [SerializeField] Animator NoCoinsAnim;

    GameObject ItemTemplate;
    GameObject g;
    [SerializeField] Transform ShopScrollView;
    Button buyBtn;

    public Sprite PurchasedImage;
    public GameObject gameManager;
    private GameDataController gameDataController;

    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        LoadAvailableChars();

        ItemTemplate = ShopScrollView.GetChild(0).gameObject;
        int len = ShopItemsList.Count;
        for (int i = 0; i < len; i++)
        {
            g = Instantiate(ItemTemplate, ShopScrollView);
            g.transform.GetChild(0).GetComponent<Image>().sprite = ShopItemsList[i].Image;
            g.transform.GetChild(1).GetChild(0).GetComponent<TMP_Text>().text = ShopItemsList[i].Price.ToString();
            buyBtn = g.transform.GetChild(2).GetComponent<Button>();
            buyBtn.interactable = !ShopItemsList[i].IsPurchased;
            buyBtn.AddEventListener(i, OnShopItemBtnClicked);
        }
        Destroy(ItemTemplate);

        Coin.Instance.UpdateAllCoinsUIText();
    }

    private void Update()
    {
        UpdateAvailableChars();
    }

    void OnShopItemBtnClicked(int itemIndex)
    {
        if (Coin.Instance.HasEnoughCoins(ShopItemsList[itemIndex].Price))
        {
            Coin.Instance.UseCoins(ShopItemsList[itemIndex].Price);
            Debug.Log(itemIndex);
            ShopItemsList[itemIndex].IsPurchased = true;
            buyBtn = ShopScrollView.GetChild(itemIndex).GetChild(2).GetComponent<Button>();
            buyBtn.interactable = false;
            buyBtn.GetComponent<Image>().sprite = PurchasedImage;

            Coin.Instance.UpdateAllCoinsUIText();
        }
        else
        {
            NoCoinsAnim.SetTrigger("NoCoins");
            Debug.Log("No traes suficiente plata compa!!");
        }
    }

    private void LoadAvailableChars()
    {
        int len = ShopItemsList.Count;
        for (int i = 0; i < len; i++)
        {
            ShopItemsList[i].IsPurchased = gameManager.GetComponent<GameManager>().AvailableCharacters[i];
        }
    }

    private void UpdateAvailableChars()
    {
        int len = ShopItemsList.Count;
        for (int i = 0; i < len; i++)
        {
            gameManager.GetComponent<GameManager>().AvailableCharacters[i] = ShopItemsList[i].IsPurchased;
        }
    }
}
