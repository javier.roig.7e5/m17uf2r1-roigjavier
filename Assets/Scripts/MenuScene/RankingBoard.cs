using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using Newtonsoft.Json;

public class RankingBoard : MonoBehaviour
{
    #region Singlton:RankingBoard

    public static RankingBoard Instance;

	void Awake ()
	{
		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);
	}

	#endregion
    [System.Serializable]
    class RegisteredScore
    {
        public string scoreR;
        public string killedEnemiesR;
        public string dateR;
    }

    [SerializeField] List<RegisteredScore> RankingList;

    GameObject ItemTemplate;
    GameObject g;
    [SerializeField] Transform RankingScrollView;
    private GameObject rankingController;

    void Start()
    {
        rankingController = GameObject.Find("RankingController");
        string[] rankingHistory = rankingController.GetComponent<RankingController>().SelectRanking();
        int lineCount = rankingController.GetComponent<RankingController>().lineCount;

        for (int i = 0; i < lineCount -1; i++)
        {
            //RankingList.Add(RankingList[i]);
            //Debug.Log("i: " + i +" pos1: " + pos1 + " pos2: " + pos2 + " pos3: " + pos3);
            /*RankingList[i].scoreR = rankingHistory[i];
            RankingList[i].killedEnemiesR = rankingHistory[i];
            RankingList[i].dateR = rankingHistory[i];*/
            RankingList.Add(RankingList[i]);
            //Debug.Log("INICIALIZANDO...");
        }

        //RankingList = new List<RegisteredScore>(new RegisteredScore[lineCount]);

        //Debug.Log("LONGITUD: " + lineCount);
        int pos1 = 0, pos2 = 1, pos3 = 2;
        for (int i = 0; i < lineCount; i++)
        {
            //Debug.Log("i: " + i +" pos1: " + pos1 + " pos2: " + pos2 + " pos3: " + pos3);
            RankingList[i].scoreR = rankingHistory[i + pos1];
            RankingList[i].killedEnemiesR = rankingHistory[i + pos2];
            RankingList[i].dateR = rankingHistory[i + pos3];
            //Debug.Log("Score: " + RankingList[i].scoreR + " Kills: " + RankingList[i].killedEnemiesR + " Date: " + RankingList[i].dateR);
            pos1 += 2;
            pos2 += 2;
            pos3 += 2;

        }

        /*for (int i = 0; i < lineCount; i++)
        {
            //Debug.Log("i: " + i + " pos1: " + pos1 + " pos2: " + pos2 + " pos3: " + pos3);
            Debug.Log("ScoreF: " + RankingList[i].scoreR + " KillsF: " + RankingList[i].killedEnemiesR + " DateF: " + RankingList[i].dateR);

        }*/


        ItemTemplate = RankingScrollView.GetChild(0).gameObject;
        int len = RankingList.Count;
        for (int i = 0; i < len; i++)
        {
            g = Instantiate(ItemTemplate, RankingScrollView);
            g.transform.GetChild(0).GetComponent<TMP_Text>().text = RankingList[i].scoreR;
            g.transform.GetChild(1).GetComponent<TMP_Text>().text = RankingList[i].killedEnemiesR;
            g.transform.GetChild(2).GetComponent<TMP_Text>().text = RankingList[i].dateR;
        }
        Destroy(ItemTemplate);
    }
}
