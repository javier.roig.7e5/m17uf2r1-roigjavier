using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public List<Characters> characters;

    public List<Weapons> weapons;

    public List<Characters> ExtraCharacter;

    public bool[] AvailableCharacters = new bool[10] {true, false, false, false, false, false, false, false, false, false };

    public bool ShopChanges = false;
    private void Awake()
    {
        if (GameManager.Instance == null)
        {
            GameManager.Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        for (int i = 1; i < 10; i++)
        {
            if (AvailableCharacters[i])
            {
                characters.Add(ExtraCharacter[i]);
            }
        }
    }

    private void Update()
    {
        if (ShopChanges == true)
        {
            for (int i = 1; i < 10; i++)
            {
                if (AvailableCharacters[i])
                {
                    characters.Remove(ExtraCharacter[i]);
                }
            }
            for (int i = 1; i < 10; i++)
            {
                if (AvailableCharacters[i])
                {
                    characters.Add(ExtraCharacter[i]);
                }
            }
            ShopChanges = false;
        }
    }
}
