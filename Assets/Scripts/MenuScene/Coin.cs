using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Coin : MonoBehaviour
{
    #region SIngleton:Coin
    public static Coin Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    [SerializeField] TMP_Text[] allCoinsUIText;
    [SerializeField] TMP_Text CoinsUIText;

    public int Coins;

    void Start()
    {
        //UpdateAllCoinsUIText();
    }

    void Update()
    {
        //CoinHolder = GameObject.Find("Text_coins");
        
        if (GameObject.Find("Text_coins").GetComponent<TMP_Text>())
        {
            CoinsUIText = GameObject.Find("Text_coins").GetComponent<TMP_Text>();
            UpdateAllCoinsUIText();
        }
    }

    public void UseCoins(int amount)
    {
        Coins -= amount;
    }

    public bool HasEnoughCoins(int amount)
    {
        return (Coins >= amount);
    }

    public void UpdateAllCoinsUIText()
    {
        /*for (int i = 0; i < allCoinsUIText.Length; i++)
        {
            allCoinsUIText[i].text = Coins.ToString();
        }*/
        CoinsUIText.text = Coins.ToString();
    }

    public void AddCoins(int coins)
    {
        Coins += coins;
    }
}
