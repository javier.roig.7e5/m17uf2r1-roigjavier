using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBehaviour : MonoBehaviour
{
    private GameObject ItemDropper;
    private ItemDropSpawner itemDropSpawner;
    [SerializeField] private int Objectdamage = 2;
    void Start()
    {
        ItemDropper = GameObject.Find("ItemDropSpawner");
        itemDropSpawner = ItemDropper.GetComponent<ItemDropSpawner>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            if (Objectdamage < 1)
            {
                itemDropSpawner.DropItem(new Vector3(transform.position.x, transform.position.y, 0));
                Destroy(gameObject);
            }
            Objectdamage--;
        }
    }
}
