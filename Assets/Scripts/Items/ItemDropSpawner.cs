using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDropSpawner : MonoBehaviour
{
    public List<GameObject> SpawnableItems;

    int rnd;

    public void DropItem(Vector3 dropPoint)
    {
        rnd = Random.Range(0, SpawnableItems.Count);
        Instantiate(SpawnableItems[rnd], dropPoint, Quaternion.identity);
    }
}
