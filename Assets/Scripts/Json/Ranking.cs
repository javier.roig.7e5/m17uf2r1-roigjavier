using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ranking
{
    public float Score;
    public int KilledEnemies;
    public string Date;
}
