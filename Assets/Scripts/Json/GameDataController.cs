using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameDataController : MonoBehaviour
{
    public GameObject gameManager;
    public GameObject coins;
    public string FileSave;
    public GameData gameData = new GameData();

    private void Awake()
    {
        FileSave = Application.dataPath + "/Scripts/Json/GameData.json";

        gameManager = GameObject.Find("GameManager");
        coins = GameObject.Find("_Coins");
        LoadData();
    }

    private void Update()
    {
        SaveData();
        LoadData();
        gameManager.GetComponent<GameManager>().ShopChanges = true;
    }

    private void LoadData()
    {
        if (File.Exists(FileSave))
        {
            string content = File.ReadAllText(FileSave);
            gameData = JsonUtility.FromJson<GameData>(content);

            gameManager.GetComponent<GameManager>().AvailableCharacters = gameData.BuyedChars;
            coins.GetComponent<Coin>().Coins = gameData.Coin;

        }
        else
        {
            //Debug.Log("File doesn't exists");
        }
    }

    private void SaveData()
    {
        GameData newData = new GameData()
        {
            BuyedChars = gameManager.GetComponent<GameManager>().AvailableCharacters,
            Coin = coins.GetComponent<Coin>().Coins

        };

        string JSONchain = JsonUtility.ToJson(newData);

        File.WriteAllText(FileSave, JSONchain);

        //Debug.Log("File Saved");
    }
}
