using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;

public class RankingController : MonoBehaviour
{
    public string FileSave;
    public Ranking ranking = new Ranking();

    public int lineCount = 0;

    public int killedEnemies;
    public float score;
    public string date;

    private void Awake()
    {
        FileSave = Application.dataPath + "/Scripts/Json/Ranking.json";

    }

    public void SaveRankingData()
    {
        Ranking newData = new Ranking()
        {
            Score = score,
            KilledEnemies = killedEnemies,
            Date = date

        };

        string JSONchain = JsonUtility.ToJson(newData);

        File.AppendAllText(FileSave, JSONchain + "\n");
    }

    public string[] SelectRanking()
    {
        lineCount = 0;
        using (var reader = File.OpenText(FileSave))
        {
            while (reader.ReadLine() != null)
            {
                lineCount++;
            }
        }
        //Debug.Log("N de lineas: " + lineCount);

        string scoreString, killedEnemiesString, dateString;
        string[] rankingHistory = new string[lineCount * 3];
        int arrayPos = 0;

        List<Ranking> ranking1 = new List<Ranking>();
        using (StreamReader jsonStream = File.OpenText(FileSave))
        {
            string line;
            while ((line = jsonStream.ReadLine()) != null)
            {
                Ranking ranking = JsonConvert.DeserializeObject<Ranking>(line);
                ranking1.Add(ranking);
            }
        }

        foreach (Ranking ranking in ranking1)
        {
            scoreString = ranking.Score.ToString();
            rankingHistory[arrayPos] = scoreString;
            arrayPos++;
            killedEnemiesString = ranking.KilledEnemies.ToString();
            rankingHistory[arrayPos] = killedEnemiesString;
            arrayPos++;
            dateString = ranking.Date;
            rankingHistory[arrayPos] = dateString;
            arrayPos++;
        }
        
        /*for(int i = 0; i < rankingHistory.Length; i++)
        {
            //Debug.Log("Index: " + i + "Value: " + rankingHistory[i]);
        }*/
        return rankingHistory;
    }
}
