using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GetScore : MonoBehaviour
{
    public GameObject gameWin;
    public GameObject playerDamage;
    private GameObject score;
    public GameObject enemieDeathCount;
    public GameObject rankingController;

    private bool scoreSaved;
    void Start()
    {
        scoreSaved = false;
        int playerIndex = PlayerPrefs.GetInt("PlayerIndex");
        string playerName = GameManager.Instance.characters[playerIndex].name;

        gameWin = GameObject.Find("WinController");
        playerDamage = GameObject.Find(playerName + "(Clone)");
        score = GameObject.Find("Score");
        enemieDeathCount = GameObject.Find("DeathEnemiesCounter");
        rankingController = GameObject.Find("RankingController");
    }

    void Update()
    {
        
        if (gameWin.GetComponent<GameWin>()._gameWinned || playerDamage.GetComponent<PlayerDamage>().AreDeath && scoreSaved == false)
        {
            rankingController.GetComponent<RankingController>().score = score.GetComponent<PointsCounter>().Score;
            rankingController.GetComponent<RankingController>().killedEnemies = enemieDeathCount.GetComponent<EnemiesDeathCount>().EnemyDeathCounter;
            rankingController.GetComponent<RankingController>().date = GetDateTime();
            rankingController.GetComponent<RankingController>().SaveRankingData();
            scoreSaved = true;
            //Debug.Log("AQUI EN: GetScore-Update()");
        }
    }

    private string GetDateTime()
    {
        DateTime now = DateTime.Now;
        string todayDate = now.ToString();
        return todayDate;
    }
}
